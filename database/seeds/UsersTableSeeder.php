<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'Jan',
            'lastname'  => 'Bakker',
            'email'     => 'admin@admin.nl',
            'password'  => Hash::make('Welkom01!'),
        ]);
    }
}
