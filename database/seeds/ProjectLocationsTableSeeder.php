<?php

use Illuminate\Database\Seeder;
use App\ProjectLocation;

class ProjectLocationsTableSeeder extends Seeder
{
    private $url = 'https://api.galleo.co/api/associationpages/projects?association=BDD659C1-C840-45C3-9DFA-71031488104B';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();

        foreach ($data['Items'] as $item) {
            if (!empty($item['Entity']['Fields']['City'])) {
                $city = $item['Entity']['Fields']['City'];

                if (!ProjectLocation::where('name', '=', $city)->exists()) {
                    ProjectLocation::create([
                        'name' => $city,
                        'description' => '',
                    ]);
                }
            }
        }
    }

    /**
     * Gets the data form an api
     *
     * @return mixed
     */
    private function getData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->url);

        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, true);
    }
}
