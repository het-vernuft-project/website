<?php

use App\Project;
use App\ProjectCategory;
use App\ProjectLocation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ProjectsTableSeeder extends Seeder
{
    /**
     * The api url
     *
     * @var string $url
     */
    private $url = 'https://api.galleo.co/api/associationpages/projects?association=BDD659C1-C840-45C3-9DFA-71031488104B';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();

        foreach ($data['Items'] as $item) {
            if (!empty($item['Entity']['Fields']['Name'])) {
                $project = $item['Entity']['Fields']['Name'];
                $address = $item['Entity']['Fields']['Address1'] ?? '';
                $city = $item['Entity']['Fields']['City'] ?? null;
                $category = $item['Entity']['Fields']['ProjectTopology'] ?? null;
                $construction_year = $item['Entity']['Fields']['CompletionYear'] ?? '1970';
                $image_link = $item['Entity']['Fields']['Picture_Meta'] ?? null;
                $thumbnail = null;

                if (!empty($image_link)) {
                    $thumbnail = md5(microtime()) . '.jpg';
                    if (!$this->saveThumbnailImage($image_link, $thumbnail) || !$this->saveOriginalImage($image_link, $thumbnail)) {
                        $thumbnail = null;
                    }
                }

                if (!empty($city) && !empty($category)) {
                    if (!Project::where('project_api_id', '=', $item['Entity']['Id'])->exists()) {
                        Project::create([
                            'name' => trim($project),
                            'slug' => $item['Entity']['Fields']['ReservedSlug'],
                            'address' => $address,
                            'latitude'=> $item['Entity']['Fields']['Lat'],
                            'longitude' => $item['Entity']['Fields']['Lon'],
                            'postal_code' => '1234AB',
                            'description' => $item['Entity']['Fields']['Description'],
                            'project_location_id' => ProjectLocation::where('name', '=', $city)->first()['id'],
                            'project_category_id' => ProjectCategory::where('name', '=', $category)->first()['id'],
                            'construction_year' => $construction_year === "1900" ? 1901 : $construction_year,
                            'free' => true,
                            'thumbnail' => $thumbnail,
                            'project_api_id' => $item['Entity']['Id']
                        ]);
                    }
                }
            }
        }
    }

    /**
     * Gets the data form an api
     *
     * @return mixed
     */
    private function getData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->url);

        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, true);
    }

    /**
     * Crops and saves a thumbnail version of the given image
     *
     * @param string $url
     * @param string $name
     * @return bool
     */
    private function saveThumbnailImage(string $url, string $name): bool
    {
        try {
            $image = Image::make($url);
            $image->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->encode('jpg');

            Storage::disk('public')->put('projects/thumbnails/' . $name, $image);
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Saves the original version of the given images
     *
     * @param string $url
     * @param string $name
     * @return bool
     */
    private function saveOriginalImage(string $url, string $name): bool
    {
        try {
            $image = Image::make($url);
            $image->encode('jpg');

            Storage::disk('public')->put('projects/originals/' . $name, $image);
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }
}
