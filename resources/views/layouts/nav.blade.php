<nav class="navbar navbar-light bg-white position-fixed vw-100">
    <div class="container pr-0">
        <div class="row justify-content-between w-100 align-items-center">
            <div class="col">
                <a href="{{ route('projects') }}" class="navbar-brand">
                    <img width="200" src="{{ asset('img/logo-colored.svg') }}" alt="logo">
                </a>
            </div>
            <div class="col p-0 nav-search-col">
                <form id="nav-search-form" method="GET">
                    <div class="form-group nav-search">
                        <label class="sr-only" for="search-project">Zoeken</label>
                        <div class="input-group mb-2">
                            <input id="search-project-nav" type="text" class="form-control" value="{{Request::query('name')}}" placeholder="Project zoeken">
                            <div class="input-group-append bg-primary cursor-pointer" id="search-btn">
                                    <span class="input-group-text">
                                        <i class="fas fa-search"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</nav>
<script>
    var navForm = document.getElementById('nav-search-form');
    var navSearch = document.getElementById('search-project-nav');
    var searchBtn = document.getElementById('search-btn');
    navForm.addEventListener("submit", function (e) {
        e.preventDefault();
        document.location.href = '{{ route('projects') }}?name=' + encodeURIComponent(navSearch.value);
    });
    searchBtn.addEventListener("click", function () {
        document.location.href = '{{ route('projects') }}?name=' + encodeURIComponent(navSearch.value);
    });
</script>
