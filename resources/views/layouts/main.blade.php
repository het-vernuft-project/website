<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Simon Boerriger, Jur Dekker, Justin van der Kruit, Roy Oosterlee">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    @yield('extraStyleSheets')
    <title>Het Vernuft - @yield('title', 'Home')</title>
</head>
<body>
    <div id="app">
        @include('layouts.nav')
        @yield('body')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('extraJavascript')
</body>
</html>
