@extends('layouts.main')

@section('title', $project->name)

@section('extraStyleSheets')
    <link href='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('body')
    <section class="full-width full-height bg-white">
        <img src="{{ asset('storage/projects/originals/' . $project->thumbnail) }}" class="vw-100 project-image" alt="Foto van project">
        <section class="bg-white">
            <section class="bg-light pt-5 pb-5">
                <section class="container">
                    <h1 class="text-primary">{{ $project->name }}</h1>
                    <a class="text-info" href="{{ route('projects') }}"><i class="fas fa-chevron-left"></i> Terug naar projecten</a>
                </section>
            </section>
            <section class="container pt-5">
                <section class="row">
                    <section class="col-xl-8 col-sm-12">
                        <p class="pb-3">{!! str_replace('\n', '<br />', $project->description) !!}</p>
                        <br class="clearfix">
                        <div id="map" style="height: 400px; margin-bottom: 1rem"></div>
                        {{--<iframe class="w-100 pb-4" height="450" frameborder="0"
                                src="https://www.google.com/maps/embed/v1/place?key={{ env('APP_GOOGLE_MAPS_KEY') }}&q={{ $project->latitude }}, {{ $project->longitude }}"
                                allowfullscreen>
                        </iframe>--}}
                    </section>
                    <section class="col-xl-4 col-sm-12 social-media-bundle text-primary">
                        <h3>Social media</h3>
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('project', ['slug' => $project->slug])) }}&t={{ urlencode($project->name) }}">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a target="_blank" href="https://twitter.com/intent/tweet?url={{ urlencode(route('project', ['slug' => $project->slug])) }}&text={{ urlencode($project->name) }}">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a target="_blank" href="https://www.reddit.com/submit?url={{ urlencode(route('project', ['slug' => $project->slug])) }}&title={{ urlencode($project->name) }}">
                            <i class="fab fa-reddit-alien"></i>
                        </a>
                        <span class="cursor-pointer copy-url" onclick="Copy()">
                            <i class="fas fa-link"></i>
                        </span>
                    </section>
                </section>
            </section>
        </section>
    </section>
    <script>
        function Copy()
        {
            var url = "{{ route('project', ['slug' => $project->slug]) }}";
            var temp = document.createElement("input");
            document.body.appendChild(temp);
            temp.setAttribute('value', url);
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);
        }
    </script>
@endsection

@section('extraJavascript')
    <script>
        var mapboxgl = window.MapBox;
        mapboxgl.accessToken = '{{ env('APP_MAPBOX_KEY') }}';

        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [{{ $project->longitude }}, {{ $project->latitude }}],
            interactive: false,
            zoom: 13
        });

        var popup = new mapboxgl.Popup()
            .setHTML('<h3>{{ $project->name }}</h3>');

        var marker = new mapboxgl.Marker()
            .setLngLat([{{ $project->longitude }}, {{ $project->latitude }}])
            .setPopup(popup)
            .addTo(map);
    </script>
@endsection
