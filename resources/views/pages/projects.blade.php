@extends('layouts.main')

@section('title', 'Alle projecten')

@section('body')
    <div class="container projects-container" id="projecten"></div>
@endsection
