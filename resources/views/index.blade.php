<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Simon Boerriger, Jur Dekker, Justin van der Kruit, Roy Oosterlee">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"/>
    <title>Het Vernuft - Home</title>
</head>
<body>
    <div class="row m-0">
        <img src="{{asset('img/login_background.jpg')}}" class="vh-100 vw-100" alt="Background">
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 bg-danger position-absolute vh-100 overflow-auto">
            <img src="{{ asset('img/logo-white.svg') }}" width="350" height="95" class="img-fluid p-4" alt="Het Vernuft logo">
            <div class="content text-white p-4">
                <h1>Het Vernuft</h1>
                <h5>Over de NLIngenieurs web-app</h5>
                <br class="clearfix">
                <p>
                    NLingenieurs is de Nederlandse branchevereniging van advies-, management- en ingenieursbureaus. De
                    leden van NLingenieurs zijn private bedrijven, variërend van kleine bureaus tot wereldwijd
                    opererende ondernemingen, die kwalitatief hoogwaardige ingenieursdiensten leveren, op basis van
                    vrije concurrentie.
                </p>
                <p>
                    Deze webapp toont bijzondere projecten van bij NLingenieurs aangesloten bureaus, waarbij het vernuft
                    van de projecten uitgelicht wordt. Wat is er zo speciaal aan een gebouw, brug, sluis, fiets- of
                    tramverbinding, et cetera?
                </p>
                <br class="clearfix">
                <form id="form" method="get">
                    <div class="form-group">
                        <label class="sr-only" for="search-project">Zoeken</label>
                        <div class="input-group mb-2">
                            <input id="search-project" type="text" class="form-control" placeholder="Project zoeken">
                            <div class="input-group-append bg-primary cursor-pointer" id="search-button-home">
                                    <span class="input-group-text">
                                        <i class="fas fa-search"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </form>
                <br class="clearfix">
                <a class="btn pt-2 pb-2 btn-primary btn-block" href="{{ route('projects') }}">Bekijk alle projecten</a>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    var form = document.getElementById('form');
    var input = document.getElementById('search-project');
    var searchButtonHome = document.getElementById('search-button-home');

    form.addEventListener('submit', function (e) {
        e.preventDefault();
        document.location.href = '{{ route('projects') }}?name=' + encodeURIComponent(input.value);
    });
    searchButtonHome.addEventListener('click', function () {
        document.location.href = '{{ route('projects') }}?name=' + encodeURIComponent(input.value);
    });
</script>
