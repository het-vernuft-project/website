@if ($paginator->hasPages())
    <ul class="pagination justify-content-between mt-5 mb-4 align-items-center w-100 pagination-margin-left">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled pagination-btn-width mr-4">
                <span class="btn btn-block btn-primary p-3 text-white text-uppercase">Vorige</span>
            </li>
        @else
            <li class="pagination-btn-width mr-4">
                <a class="btn btn-block btn-primary p-3 text-white text-uppercase" href="{{ $paginator->previousPageUrl() }}" rel="prev">Vorige</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    {{--  Use three dots when current page is greater than 2.  --}}
                    @if ($paginator->currentPage() > 2 && $page === 2)
                        <li class="disabled"><span class="pagination-numbers">...</span></li>
                    @endif

                    {{--  Show active page else show the first and last two pages from current page.  --}}
                    @if ($page == $paginator->currentPage())
                        <li class="active text-info pagination-active font-weight-bold"><u class="pagination-numbers">{{ $page }}</u></li>
                    @elseif ($page === $paginator->currentPage() + 1 || $page === $paginator->currentPage() + 2 || $page === $paginator->currentPage() - 1 || $page === $paginator->currentPage() - 2 || $page === $paginator->lastPage() || $page === 1)
                        <li class=""><a class="pagination-numbers" href="{{ $url }}">{{ $page }}</a></li>
                    @endif

                    {{--  Use three dots when current page is awasy from end.  --}}
                    @if ($paginator->currentPage() < $paginator->count() - 2 && $page === $paginator->count() - 1)
                        <li class="disabled"><span class="pagination-numbers">...</span></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pagination-btn-width ml-4">
                <a class="btn btn-block btn-primary p-3 text-white text-uppercase" href="{{ $paginator->nextPageUrl() }}">Volgende</a>
            </li>
        @else
            <li class="pagination-btn-width disabled ml-4">
                <span class="btn btn-block btn-primary p-3 text-white text-uppercase">Volgende</span>
            </li>
        @endif
    </ul>
@endif
