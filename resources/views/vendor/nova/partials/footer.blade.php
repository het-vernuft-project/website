<p class="mt-8 text-center text-xs text-80">
    <a href="https://www.nlingenieurs.nl/" class="text-primary dim no-underline">Koninklijke NLingenieurs</a>
    <span class="px-1">&middot;</span>
    &copy; Copyright {{ date('Y') }} NLingenieur
</p>
