import React, {Component} from "react";
import ReactPaginate from 'react-paginate';

export default class Projects extends Component {

    resultsPerPage = 10;
    backup = [];

    state = {
        page: 0,
    };


    changePage(page) {
        window.scrollTo(0,0);
        this.setState({page: page});
    }

    componentWillReceiveProps() {
        this.setState({page: 0});
    }

    render() {
        const {projects, ready} = this.props;
        const total = Object.keys(projects).length;

        if (!ready) {
            return <p className="text-center text-primary mt-5">
                <i className="fas fa-spinner fa-pulse fa-5x"/>
            </p>
        }

        return <>
            {!projects.length ?
                <p className="text-center">
                    Geen projecten gevonden
                </p>
                :
                <>
                    <p className="projects-status-list">
                        <small>{total} resultaten gevonden</small>
                    </p>
                    {projects.slice(this.state.page * this.resultsPerPage, this.state.page * this.resultsPerPage + this.resultsPerPage).map((project, key) =>
                        <div key={key} className="box mb-4 ml-4 mr-2">
                            <div className="row align-items-center bg-light">
                                <img className="col-md-3 col-sm-12 project-image-container p-0" src={project.thumbnail}
                                     alt={project.name}/>
                                <div className="col px-4 py-3">
                                    <h2 className="text-primary">{project.name}</h2>
                                    <h6 className="text-info">
                                        <small>{project.city} - {project.construction_year}</small>
                                    </h6>
                                    <p>{project.description.replace(/(<([^>]+)>)/ig, "")}</p>
                                </div>
                                <div className="col-md-3 col-sm-12 px-5 project-read-more">
                                    <a href={`/project/${project.slug}`}
                                       className="btn btn-block btn-primary p-3 text-white text-uppercase">
                                        Bekijken
                                    </a>
                                </div>
                            </div>
                        </div>
                    )}
                    <ReactPaginate
                        previousLabel={'Vorige'}
                        nextLabel={'Volgende'}
                        forcePage={this.state.page}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        pageCount={total / this.resultsPerPage}
                        marginPagesDisplayed={1}
                        pageRangeDisplayed={2}
                        onPageChange={e => this.changePage(e.selected)}
                        containerClassName={'pagination'}
                        subContainerClassName={'pages pagination'}
                        activeClassName={'active'}
                    />
                </>
            }
        </>
    }
}
