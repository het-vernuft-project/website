import React, {Component} from "react";

export default class Filters extends Component {

    collect = {
        "free": true,
        "construction-type": null,
        "sort": null,
        "location": null,
        "build-year-start": null,
        "build-year-end": null,
    };

    register(checkbox, e) {
        this.collect[e.target.name] = checkbox ? e.target.checked : e.target.value;
        this.props.submit(this.collect);
    }

    render() {
        const { categories, locations } = this.props;

        $('.collapse').collapse({
            toggle: false,
        });

        return <>
            <div className="projects-filters-mobile">
                <p>
                    <button className="btn btn-block btn-primary" type="button" data-toggle="collapse" data-target="#filers">
                        Filters &nbsp;<i className="fas fa-filter"/>
                    </button>
                </p>
                <div className="collapse" id="filers">
                    <div className="box px-4 py-3 bg-light mr-4 projects-filters projects-filters-mobile">
                        <h2>Filteren</h2>
                        <div className="form-check">
                            <input className="form-check-input" type="checkbox" onChange={this.register.bind(this, true)} defaultChecked value="1" id="free-access"
                                   name="free"/>
                            <label className="form-check-label" htmlFor="free-access">
                                Gratis toegang
                            </label>
                        </div>
                        <br/>
                        <div className="form-group">
                            <label htmlFor="construction-type">Type constructie</label>
                            <select onChange={this.register.bind(this, false)} className="form-control" name="construction-type" id="construction-type">
                                <option value="">-</option>
                                {categories.map((category, key) => <option key={key} value={category.id}>{category.name}</option>)}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="sort">Sorteren op</label>
                            <select onChange={this.register.bind(this, false)} className="form-control" name="sort" id="sort">
                                <option defaultChecked value="">-</option>
                                <option value="alphabet">Alfabet</option>
                                <option value="new">Nieuw toegevoegd</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="location">Locatie</label>
                            <select onChange={this.register.bind(this, false)} className="form-control" name="location" id="location">
                                <option defaultChecked value="">-</option>
                                {locations.map((location, key) => <option key={key} value={location.id}>{location.name}</option>)}
                            </select>
                        </div>
                        <div className="form-group">
                            <label>Bouwjaar</label>
                            <input className="form-control mb-1" type="number" onChange={this.register.bind(this, false)} name="build-year-start" min="0"
                                   max="2050" placeholder="0"/>
                            <input className="form-control" type="number" onChange={this.register.bind(this, false)} name="build-year-end" min="0" max="2050"
                                   placeholder="2020"/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="box px-4 py-3 bg-light mr-4 projects-filters">
                <h2>Filteren</h2>
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" onChange={this.register.bind(this, true)} defaultChecked value="1" id="free-access"
                           name="free"/>
                    <label className="form-check-label" htmlFor="free-access">
                        Gratis toegang
                    </label>
                </div>
                <br/>
                <div className="form-group">
                    <label htmlFor="construction-type">Type constructie</label>
                    <select onChange={this.register.bind(this, false)} className="form-control" name="construction-type" id="construction-type">
                        <option value="">-</option>
                        {categories.map((category, key) => <option key={key} value={category.id}>{category.name}</option>)}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="sort">Sorteren op</label>
                    <select onChange={this.register.bind(this, false)} className="form-control" name="sort" id="sort">
                        <option defaultChecked value="">-</option>
                        <option value="alphabet">Alfabet</option>
                        <option value="new">Nieuw toegevoegd</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="location">Locatie</label>
                    <select onChange={this.register.bind(this, false)} className="form-control" name="location" id="location">
                        <option defaultChecked value="">-</option>
                        {locations.map((location, key) => <option key={key} value={location.id}>{location.name}</option>)}
                    </select>
                </div>
                <div className="form-group">
                    <label>Bouwjaar</label>
                    <input className="form-control mb-1" type="number" onChange={this.register.bind(this, false)} name="build-year-start" min="0"
                           max="2050" placeholder="0"/>
                    <input className="form-control" type="number" onChange={this.register.bind(this, false)} name="build-year-end" min="0" max="2050"
                           placeholder="2020"/>
                </div>
            </div>
        </>;
    }
}
