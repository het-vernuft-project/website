import React  from 'react';
import ReactDOM from 'react-dom';
import Projects from './Projects';

if (document.getElementById('projecten')) {
    ReactDOM.render(<Projects />, document.getElementById('projecten'));
}
