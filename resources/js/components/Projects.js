import React, {Component} from "react";

import Filters from "./Projects/Filters";
import Show from "./Projects/Show";

export default class Projects extends Component {

    state = {
        categories: [],
        locations: [],
        projects: [],
        search: [],
        pagination: [],
        searchFilter: new URLSearchParams(window.location.search),
        ready: false,
        filteredProjects: [],
        noResultOnFilter: false,
    };

    getProjects() {
        fetch(`//api.${process.env.MIX_APP_DOMAIN}/projecten/zoeken`, {
            body: JSON.stringify({name: this.state.searchFilter.get('name')}),
            method: 'POST',
            mode: 'cors',
            cache: 'force-cache',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(response => Promise.resolve(response.json()))
            .then(result => {
                if (result.status === 200) {
                    this.setState({
                        projects: result.data,
                        ready: true,
                    });
                } else {
                    Promise.reject('Status did not send OK status.');
                }
            })
    }

    componentDidMount() {
        fetch(`//api.${process.env.MIX_APP_DOMAIN}/projecten/categorieen`)
            .then(response => Promise.resolve(response.json()))
            .then(result => {
                if (result.status === 200) {
                    this.setState({
                        categories: result.data,
                    });
                } else {
                    Promise.reject('Status did not send OK status.');
                }
            });

        fetch(`//api.${process.env.MIX_APP_DOMAIN}/projecten/locaties`)
            .then(response => Promise.resolve(response.json()))
            .then(result => {
                if (result.status === 200) {
                    this.setState({
                        locations: result.data,
                    });
                } else {
                    Promise.reject('Status did not send OK status.');
                }
            });

        this.getProjects();
    }

    filters(values) {
        let collect = this.state.projects;
        Object.entries(values).map(item => {
            switch (item[0]) {
                case "free":
                    collect = collect.filter(data => data.free === (item[1] ? 1 : 0));
                    break;
                case "construction-type":
                    if (item[1] !== "" && item[1] !== null) {
                        collect = collect.filter(data => data['construction_type'] === Number(item[1]));
                    }
                    break;
                case "location":
                    if (item[1] !== "" && item[1] !== null) {
                        collect = collect.filter(data => data.location === Number(item[1]));
                    }
                    break;
                case "build-year-start":
                    if (item[1] !== "" && item[1] !== null) {
                        collect = collect.filter(data => data['construction_year'] > item[1]);
                    }
                    break;
                case "build-year-end":
                    if (item[1] !== "" && item[1] !== null) {
                        collect = collect.filter(data => data['construction_year'] < item[1]);
                    }
                    break;
                default:
                    break;
            }
        });

        if (values['sort']) {
            if (values['sort'] === 'alphabet') {
                collect = collect.sort((a, b) => a.name.localeCompare(b.name));
            } else if(values['sort'] === 'new') {
                collect = collect.sort((a, b) => (a.created_at < b.created_at) ? 1 : -1);
            }
        }
        if (Object.entries(collect).length) {
            this.setState({
                filteredProjects: collect,
                noResultOnFilter: false,
            })
        } else {
            this.setState({
                filteredProjects: [],
                noResultOnFilter: true,
            })
        }
    }

    render() {
        const {categories, locations, projects, ready, filteredProjects, noResultOnFilter} = this.state;

        return <div className="container">
            <div className="row justify-content-center">
                <div className="col-xl-3 col-lg-4 col-md-10 col-sm-12">
                    <Filters
                        categories={categories}
                        locations={locations}
                        submit={this.filters.bind(this)}
                    />
                </div>
                <div className="col-xl-9 col-lg-8 col-md-12 projects-list">
                    <Show
                        projects={Object.keys(filteredProjects).length ? filteredProjects : noResultOnFilter ? [] : projects}
                        ready={ready}
                    />
                </div>
            </div>
        </div>;
    }
}
