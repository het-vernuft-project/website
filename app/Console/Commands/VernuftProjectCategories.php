<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ProjectCategory;

class VernuftProjectCategories extends Command
{
    private $url = 'https://api.galleo.co/api/associationpages/projects?association=BDD659C1-C840-45C3-9DFA-71031488104B';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vernuft:project-categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load all project categories from Het Vernuft API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line("Importing all Het Vernuft project categories from API");

        $data = $this->getData();

        $bar = $this->output->createProgressBar($data['Count']);

        $added_count = 0;

        foreach ($data['Items'] as $item) {
            if (!empty($item['Entity']['Fields']['ProjectTopology'])) {
                $category = $item['Entity']['Fields']['ProjectTopology'];

                if (!ProjectCategory::where('name', '=', $category)->exists()) {
                    ProjectCategory::create([
                        'name' => $category,
                        'description' => '',
                    ]);
                    $added_count++;
                    $bar->advance();
                } else {
                    $bar->advance();
                }
            } else {
                $bar->advance();
            }
        }

        $bar->finish();
        $this->line('');
        $this->info("Added " . $added_count . " Project Categories");
    }

    /**
     * Gets the data form an api
     *
     * @return mixed
     */
    private function getData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->url);

        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, true);
    }
}
