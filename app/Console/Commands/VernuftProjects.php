<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use App\Project;
use App\ProjectCategory;
use App\ProjectLocation;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class VernuftProjects extends Command
{
    private $url = 'https://api.galleo.co/api/associationpages/projects?association=BDD659C1-C840-45C3-9DFA-71031488104B';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vernuft:projects {force?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load all projects from Het Vernuft API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line("Importing all Het Vernuft projects from API");

        $data = $this->getData();

        $bar = $this->output->createProgressBar($data['Count']);

        $added_count = 0;

        foreach ($data['Items'] as $item) {
            if (!empty($item['Entity']['Fields']['Name'])) {
                $project = $item['Entity']['Fields']['Name'];
                $address = $item['Entity']['Fields']['Address1'] ?? '';
                $city = $item['Entity']['Fields']['City'] ?? null;
                $category = $item['Entity']['Fields']['ProjectTopology'] ?? null;
                $construction_year = $item['Entity']['Fields']['CompletionYear'] ?? '1970';
                $image_link = $item['Entity']['Fields']['Picture_Meta'] ?? null;
                $thumbnail = null;

                if (!empty($city) && !empty($category) && ProjectLocation::where('name', '=', $city)->exists() && ProjectCategory::where('name', '=', $category)->exists()) {
                    if (!Project::where('project_api_id', '=', $item['Entity']['Id'])->exists()) {
                        if (!empty($image_link)) {
                            $thumbnail = md5(microtime()) . '.jpg';
                            if (!$this->saveThumbnailImage($image_link, $thumbnail) || !$this->saveOriginalImage($image_link, $thumbnail)) {
                                $thumbnail = null;
                            }
                        }

                        Project::create([
                            'name' => trim($project),
                            'slug' => $item['Entity']['Fields']['ReservedSlug'],
                            'address' => $address,
                            'latitude'=> $item['Entity']['Fields']['Lat'],
                            'longitude' => $item['Entity']['Fields']['Lon'],
                            'postal_code' => '1234AB',
                            'description' => $item['Entity']['Fields']['Description'],
                            'project_location_id' => ProjectLocation::where('name', '=', $city)->first()['id'],
                            'project_category_id' => ProjectCategory::where('name', '=', $category)->first()['id'],
                            'construction_year' => $construction_year === "1900" ? 1901 : $construction_year,
                            'free' => true,
                            'thumbnail' => $thumbnail,
                            'project_api_id' => $item['Entity']['Id']
                        ]);
                        $added_count++;
                        $bar->advance();
                    } else {
                        if($this->argument('force')){
                            if (!empty($image_link)) {
                                $thumbnail = md5(microtime()) . '.jpg';
                                if (!$this->saveThumbnailImage($image_link, $thumbnail) || !$this->saveOriginalImage($image_link, $thumbnail)) {
                                    $thumbnail = null;
                                }
                            }

                            $project = Project::where('project_api_id', '=', $item['Entity']['Id'])->update([
                                'name' => trim($project),
                                'slug' => $item['Entity']['Fields']['ReservedSlug'],
                                'address' => $address,
                                'latitude'=> $item['Entity']['Fields']['Lat'],
                                'longitude' => $item['Entity']['Fields']['Lon'],
                                'postal_code' => '1234AB',
                                'description' => $item['Entity']['Fields']['Description'],
                                'project_location_id' => ProjectLocation::where('name', '=', $city)->first()['id'],
                                'project_category_id' => ProjectCategory::where('name', '=', $category)->first()['id'],
                                'construction_year' => $construction_year === "1900" ? 1901 : $construction_year,
                                'free' => true,
                                'thumbnail' => $thumbnail,
                                'project_api_id' => $item['Entity']['Id']
                            ]);
                        }

                        $bar->advance();
                    }
                } else {
                    $bar->advance();
                }

            } else {
                $bar->advance();
            }
        }

        $bar->finish();
        $this->line('');
        $this->info("Added " . $added_count . " Projects");
    }

    /**
     * Gets the data form an api
     *
     * @return mixed
     */
    private function getData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->url);

        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, true);
    }

    /**
     * Crops and saves a thumbnail version of the given image
     *
     * @param string $url
     * @param string $name
     * @return bool
     */
    private function saveThumbnailImage(string $url, string $name): bool
    {
        try {
            $image = Image::make($url);
            $image->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->encode('jpg');

            Storage::disk('public')->put('projects/thumbnails/' . $name, $image);
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Saves the original version of the given images
     *
     * @param string $url
     * @param string $name
     * @return bool
     */
    private function saveOriginalImage(string $url, string $name): bool
    {
        try {
            $image = Image::make($url);
            $image->encode('jpg');

            Storage::disk('public')->put('projects/originals/' . $name, $image);
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }
}
