<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Project;
use App\ProjectCategory;
use App\ProjectLocation;
use App\Transformers\ProjectsTransformer;
use App\Transformers\ProjectTransformer;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Returns all projects in the database
     *
     * @return JsonResponse
     */
    public function index()
    {
        return responder()
            ->success(Project::get(), ProjectsTransformer::class)
            ->respond();
    }

    /**
     * Returns project from the database by id
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return responder()
            ->success(Project::findOrFail($id), ProjectTransformer::class)
            ->respond();
    }

    /**
     * Returns all project categories from the database
     *
     * @return JsonResponse
     */
    public function categories()
    {
        return responder()
            ->success(ProjectCategory::get())
            ->respond();
    }

    /**
     * Returns all project locations from the database
     *
     * @return JsonResponse
     */
    public function locations()
    {
        return responder()
            ->success(ProjectLocation::get())
            ->respond();
    }

    /**
     * Gets the projects with search query
     *
     * @param Request $request
     * @return Builder
     */
    public function search(Request $request)
    {
        $projects = Project::query();

        if (!empty($request->get('name'))) {
            $projects->where('name', 'LIKE', '%' . $request->get('name') . '%');
        }

        return responder()
            ->success($projects, ProjectsTransformer::class)
            ->respond();
    }
}
