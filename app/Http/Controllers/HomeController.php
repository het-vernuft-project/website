<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * Display a the landing page.
     *
     * @return Response
     */
    public function index()
    {
        return view('index');
    }
}
