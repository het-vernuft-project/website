<?php

namespace App\Http\Controllers;

use App\Project;
use App\Transformers\ProjectsTransformer;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('pages.projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return Response
     */
    public function show(string $slug)
    {

        $project = Project::where('slug', $slug)->firstOrFail();

        return view('pages.project', [
            'project' => $project,
        ]);
    }
}
