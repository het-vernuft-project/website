<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Trix;

class Project extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Project';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Name')->rules('required', 'max:255'),

            Text::make('Slug')->withMeta(['extraAttributes' => [
                'readonly' => true
            ]])->hideFromIndex(),

            Text::make('Address')->rules('required')->hideFromIndex(),

            Text::make('Postal code')->rules('required')->hideFromIndex(),

            Number::make('Latitude')->rules('required', 'max:10')->hideFromIndex(),

            Number::make('Longitude')->rules('required', 'max:10')->hideFromIndex(),

            Trix::make('Description')->rules('required')->hideFromIndex(),

            Number::make('Construction year')->rules('required')->hideFromIndex(),

            Boolean::make('Free')->hideFromIndex(),

            Image::make('Thumbnail')->disk('public')->rules('required')->hideFromIndex(),

            BelongsTo::make('ProjectLocation', 'project_location'),
            
            BelongsTo::make('ProjectCategory', 'project_category'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function label() {
        return 'Projects';
    }
}
