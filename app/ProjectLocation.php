<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class ProjectLocation extends Model
{
    protected $fillable = ['name', 'description'];

    public function project()
    {
        return $this->hasMany(Project::class);
    }
}
