<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProjectLocation;
use App\ProjectCategory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Project extends Model
{
    use HasSlug;

    protected $fillable = ['name', 'address', 'latitude', 'longitude', 'postal_code', 'description', 'construction_year', 'free', 'thumbnail', 'project_location_id', 'project_category_id', 'project_api_id'];

    public function project_location()
    {
        return $this->belongsTo(ProjectLocation::class);
    }

    public function project_category()
    {
        return $this->belongsTo(ProjectCategory::class);
    }
    
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
