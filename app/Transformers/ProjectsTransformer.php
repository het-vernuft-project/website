<?php

namespace App\Transformers;

use App\Project;
use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProjectsTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  Project $project
     * @return array
     */
    public function transform(Project $project)
    {
        return [
            'id' => (int) $project->id,
            'name' => (string) $project->name,
            'free' => $project->free,
            'construction_year' => (int) $project->construction_year,
            'construction_type' => $project->project_category_id,
            'location' => $project->project_location_id,
            'city' => $project->project_location->name,
            'slug' => $project->slug,
            'description' => Str::limit($project->description, 200),
            'thumbnail' => Storage::disk('public')->url('projects/thumbnails/' . $project->thumbnail),
            'created_at' => strtotime($project->created_at)
        ];
    }
}
