<?php

namespace App\Transformers;

use App\Route;
use Flugg\Responder\Transformers\Transformer;

class RoutesTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Route $route
     * @return array
     */
    public function transform(Route $route)
    {
        return [
            'id' => (int) $route->id,
            'name' => (string) $route->name,
            'description' => $route->description,
            'projects_count' => count($route->projects),
            'created_at' => $route->created_at
        ];
    }
}
