<?php

namespace App\Transformers;

use App\Route;
use Flugg\Responder\Transformers\Transformer;

class RouteTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Route $route
     * @return array
     */
    public function transform(Route $route)
    {
        $projects = [];
        foreach($route->projects as $project){
            $projects[] = (object)[
                'project_id' => $project->id,
                'project_name' => $project->name,
                'latitude' => $project->latitude,
                'longitude' => $project->longitude,
            ];
        }
        return [
            'id' => (int) $route->id,
            'name' => (string) $route->name,
            'description' => $route->description,
            'locations' => $projects,
        ];
    }
}
