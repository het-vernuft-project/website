<?php

namespace App\Transformers;

use App\Project;
use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Facades\Storage;

class ProjectTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Project $project
     * @return array
     */
    public function transform(Project $project)
    {
        return [
            'id' => (int) $project->id,
            'name' => (string) $project->name,
            'slug' => (string) $project->slug,
            'address' => (string) $project->address,
            'postal_code' => (string) $project->postal_code,
            'latitude' => $project->latitude,
            'longitude' => $project->longitude,
            'description' => $project->description,
            'project_location' => $project->project_location,
            'project_category' => $project->project_category,
            'construction_year' => (int) $project->construction_year,
            'free' => $project->free,
            'thumbnail' => Storage::disk('public')->url('projects/thumbnails/' . $project->thumbnail),
        ];
    }
}
