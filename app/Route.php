<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = ['name', 'description', 'thumbnail'];

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_routes', 'route_id', 'project_id');
    }
}
