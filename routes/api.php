<?php

use App\Project;
use App\ProjectCategory;
use App\ProjectLocation;
use App\Transformers\ProjectsTransformer;
use App\Transformers\ProjectTransformer;
use App\Route as R;
use App\Transformers\RoutesTransformer;
use App\Transformers\RouteTransformer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['domain' => 'api.' . env('APP_DOMAIN')], function () {
    Route::get('/projects', "Api\ProjectController@index");
    Route::get('/project/{id}', "Api\ProjectController@show");
    Route::get('/projecten/categorieen', "Api\ProjectController@categories");
    Route::get('/projecten/locaties', "Api\ProjectController@locations");
    Route::post('/projecten/zoeken', "Api\ProjectController@search");

    Route::get('/routes', function() {
        return responder()->success(R::get(), RoutesTransformer::class)->respond();
    });

    Route::get('/route/{id}', function($id) {
        return responder()->success(R::findOrFail($id), RouteTransformer::class)->respond();
    });
});