<?php

Route::get('/', 'HomeController@index')->name('index');
Route::get('/projecten', "ProjectController@index")->name('projects');
Route::get('/project/{slug}', "ProjectController@show")->name('project');
